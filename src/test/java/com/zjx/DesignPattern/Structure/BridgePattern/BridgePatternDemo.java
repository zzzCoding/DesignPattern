package com.zjx.DesignPattern.Structure.BridgePattern;

import com.zjx.DesignPattern.Structure.BridgePattern.impl.GreenCircle;
import com.zjx.DesignPattern.Structure.BridgePattern.impl.RedCircle;

public class BridgePatternDemo {
	public static void main(String[] args) {
		Shape redCircle = new Circle(100, 100, 10, new RedCircle());
		Shape greenCircle = new Circle(100, 100, 10, new GreenCircle());

		redCircle.draw();
		greenCircle.draw();
	}
}
