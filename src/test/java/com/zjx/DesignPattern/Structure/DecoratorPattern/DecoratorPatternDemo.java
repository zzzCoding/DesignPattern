package com.zjx.DesignPattern.Structure.DecoratorPattern;

import com.zjx.DesignPattern.Structure.DecoratorPattern.impl.Circle;
import com.zjx.DesignPattern.Structure.DecoratorPattern.impl.Rectangle;

public class DecoratorPatternDemo extends Test1{

	public DecoratorPatternDemo(int t) {
		super(t);
	}

	public static void main(String[] args) {
		Shape circle = new Circle();
		Shape redCircle = new RedShapeDecorator(new Circle());
		Shape redRectangle = new RedShapeDecorator(new Rectangle());
		
		System.out.println("Circle with normal border");
		circle.draw();
		
		System.out.println("\nCircle of red border");
		redCircle.draw();
		
		System.out.println("\nRectangle of red border");
		redRectangle.draw();
	}
}


class Test1{
	int t;
	public Test1(int t){
		this.t = t;
	}
	
}
