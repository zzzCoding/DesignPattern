/**
 * @Project apacheTest
 * @File InterpreterPatternDemo.java
 * @Package com.zzz.pattern.Interpreter
 * @Date 2017年2月8日 上午11:03:01
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.interpreter;

import com.zjx.DesignPattern.Behavior.interpreter.impl.AndExpression;
import com.zjx.DesignPattern.Behavior.interpreter.impl.OrExpression;
import com.zjx.DesignPattern.Behavior.interpreter.impl.TerminalExpression;

/**
 * @ClassName InterpreterPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:03:01
 */
public class InterpreterPatternDemo {
	// 规则：Robert 和 John 是男性
	public static Expression getMaleExpression() {
		Expression robert = new TerminalExpression("Robert");
		Expression john = new TerminalExpression("John");
		return new OrExpression(robert, john);
	}

	// 规则：Julie 是一个已婚的女性
	public static Expression getMarriedWomanExpression() {
		Expression julie = new TerminalExpression("Julie");
		Expression married = new TerminalExpression("Married");
		return new AndExpression(julie, married);
	}

	public static void main(String[] args) {
		Expression isMale = getMaleExpression();
		Expression isMarriedWoman = getMarriedWomanExpression();

		System.out.println("John is male? " + isMale.interpret("John"));
		System.out.println("Julie is a married women? " + isMarriedWoman.interpret("Married Julie"));
	}
}
