/**
 * @Project apacheTest
 * @File CommandPatternDemo.java
 * @Package com.zzz.pattern.command
 * @Date 2017年2月8日 上午10:38:48
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.command;

import com.zjx.DesignPattern.Behavior.command.impl.BuyStock;
import com.zjx.DesignPattern.Behavior.command.impl.OtherStock;
import com.zjx.DesignPattern.Behavior.command.impl.SellStock;

/**
 * @ClassName CommandPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:38:48
 */
public class CommandPatternDemo {

	public static void main(String[] args) {
		Stock abcStock = new Stock();

		BuyStock buyStockOrder = new BuyStock(abcStock);
		SellStock sellStockOrder = new SellStock(abcStock);
		OtherStock otherStock = new OtherStock(abcStock);

		Broker broker = new Broker();
		broker.takeOrder(buyStockOrder);
		broker.takeOrder(sellStockOrder);
		broker.takeOrder(otherStock);

		broker.placeOrders();
	}
}
