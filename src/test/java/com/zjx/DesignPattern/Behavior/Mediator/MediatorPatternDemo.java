/**
 * @Project apacheTest
 * @File MediatorPatternDemo.java
 * @Package com.zzz.pattern.Mediator
 * @Date 2017年2月8日 下午12:01:17
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Mediator;

import com.zjx.DesignPattern.Behavior.Mediator.entity.User;

/**
 * @ClassName MediatorPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午12:01:17
 */
public class MediatorPatternDemo {
	public static void main(String[] args) {
		User robert = new User("Robert");
		User john = new User("John");

		robert.sendMessage("Hi! John!");
		john.sendMessage("Hello! Robert!");
	}
}
