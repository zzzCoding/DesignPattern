/**
 * @Project apacheTest
 * @File IteratorPatternDemo.java
 * @Package com.zzz.pattern.Iterator
 * @Date 2017年2月8日 上午11:26:29
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Iterator;

import java.util.ArrayList;
import java.util.List;

import com.zjx.DesignPattern.Behavior.Iterator.impl.NameRepository;

/**
 * @ClassName IteratorPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:26:29
 */
public class IteratorPatternDemo {

	public static void main(String[] args) {
		NameRepository namesRepository = new NameRepository();

		for (Iterator iter = namesRepository.getIterator(); iter.hasNext();) {
			String name = (String) iter.next();
			System.out.println("Name : " + name);
		}

	}

	public void listIterator() {
		List<String> l = new ArrayList<String>();
		l.add("aa");
		l.add("bb");
		l.add("cc");
		java.util.Iterator<String> iter = l.iterator();
		while (iter.hasNext()) {
			String str = (String) iter.next();
			System.out.println(str);
		}
	}
}
