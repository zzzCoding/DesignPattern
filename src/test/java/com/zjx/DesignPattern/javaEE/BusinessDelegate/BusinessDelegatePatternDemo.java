/**
 * @Project apacheTest
 * @File BusinessDelegatePatternDemo.java
 * @Package com.zzz.pattern.BusinessDelegate
 * @Date 2017年2月8日 下午3:13:32
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.BusinessDelegate;
/**
 * @ClassName BusinessDelegatePatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:13:32
 */
public class BusinessDelegatePatternDemo {
	public static void main(String[] args) {

		BusinessDelegate businessDelegate = new BusinessDelegate();
		businessDelegate.setServiceType("EJB");

		Client client = new Client(businessDelegate);
		client.doTask();

		businessDelegate.setServiceType("JMS");
		client.doTask();
	}
}
