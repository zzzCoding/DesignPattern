/**
 * @Project apacheTest
 * @File MVCPatternDemo.java
 * @Package com.zzz.pattern.MVC
 * @Date 2017年2月8日 下午2:59:33
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.MVC;

/**
 * @ClassName MVCPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午2:59:33
 */
public class MVCPatternDemo {
	public static void main(String[] args) {

		// 从数据可获取学生记录
		Student model = retriveStudentFromDatabase();

		// 创建一个视图：把学生详细信息输出到控制台
		StudentView view = new StudentView();

		StudentController controller = new StudentController(model, view);
		controller.updateView();
		// 更新模型数据
		controller.setStudentName("John");
		controller.updateView();
	}

	private static Student retriveStudentFromDatabase() {
		Student student = new Student();
		student.setName("Robert");
		student.setRollNo("10");
		return student;
	}
}
