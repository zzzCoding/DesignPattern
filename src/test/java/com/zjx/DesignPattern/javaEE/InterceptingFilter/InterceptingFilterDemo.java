/**
 * @Project apacheTest
 * @File InterceptingFilterDemo.java
 * @Package com.zzz.pattern.InterceptingFilter
 * @Date 2017年2月8日 下午5:12:19
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter;

import com.zjx.DesignPattern.javaEE.InterceptingFilter.filter.AuthenticationFilter;
import com.zjx.DesignPattern.javaEE.InterceptingFilter.filter.DebugFilter;

/**
 * @ClassName InterceptingFilterDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:12:19
 */
public class InterceptingFilterDemo {
	public static void main(String[] args) {
		FilterManager filterManager = new FilterManager(new Target());
		filterManager.setFilter(new AuthenticationFilter());
		filterManager.setFilter(new DebugFilter());

		Client client = new Client();
		client.setFilterManager(filterManager);
		client.sendRequest("HOME");
	}
}
