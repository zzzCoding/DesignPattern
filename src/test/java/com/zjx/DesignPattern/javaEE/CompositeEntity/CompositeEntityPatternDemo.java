/**
 * @Project apacheTest
 * @File CompositeEntityPatternDemo.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午4:01:59
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity;

/**
 * @ClassName CompositeEntityPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:01:59
 */
public class CompositeEntityPatternDemo {
	public static void main(String[] args) {
		Client client = new Client();
		client.setData("Test", "Datax");
		client.printData();
		client.setData("Second Test", "Data1");
		client.printData();
	}
}
