/**
 * @Project apacheTest
 * @File FrontControllerPatternDemo.java
 * @Package com.zzz.pattern.FrontController
 * @Date 2017年2月8日 下午4:35:53
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.FrontController;
/**
 * @ClassName FrontControllerPatternDemo
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:35:53
 */
public class FrontControllerPatternDemo {
	public static void main(String[] args) {
		FrontController frontController = new FrontController();
		frontController.dispatchRequest("HOME");
		frontController.dispatchRequest("STUDENT");
	}
}
