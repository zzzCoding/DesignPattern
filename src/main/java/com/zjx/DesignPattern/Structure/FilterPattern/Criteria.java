package com.zjx.DesignPattern.Structure.FilterPattern;

import java.util.List;

import com.zjx.DesignPattern.Structure.FilterPattern.entity.Person;

public interface Criteria {

	public List<Person> meetCriteria(List<Person> persons);
}
