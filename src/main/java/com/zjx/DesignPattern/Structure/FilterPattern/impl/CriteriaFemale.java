package com.zjx.DesignPattern.Structure.FilterPattern.impl;

import java.util.ArrayList;
import java.util.List;

import com.zjx.DesignPattern.Structure.FilterPattern.Criteria;
import com.zjx.DesignPattern.Structure.FilterPattern.entity.Person;

public class CriteriaFemale implements Criteria {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> femalePersons = new ArrayList<Person>();
		for (Person person : persons) {
			if (person.getGender().equalsIgnoreCase("FEMALE")) {
				femalePersons.add(person);
			}
		}
		return femalePersons;
	}

}
