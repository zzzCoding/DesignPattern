package com.zjx.DesignPattern.Structure.DecoratorPattern.impl;

import com.zjx.DesignPattern.Structure.DecoratorPattern.Shape;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("Shape: Circle");
	}
}