package com.zjx.DesignPattern.Structure.DecoratorPattern;

public interface Shape {
	void draw();
}
