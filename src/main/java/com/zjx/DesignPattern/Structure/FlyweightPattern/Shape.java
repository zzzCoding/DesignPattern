package com.zjx.DesignPattern.Structure.FlyweightPattern;

public interface Shape {

	void draw();
}
