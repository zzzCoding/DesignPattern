package com.zjx.DesignPattern.Structure.FacadePattern;

public interface Shape {
	void draw();
}
