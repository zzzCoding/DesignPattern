package com.zjx.DesignPattern.Structure.FacadePattern.impl;

import com.zjx.DesignPattern.Structure.FacadePattern.Shape;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Square::draw()");
	}

}
