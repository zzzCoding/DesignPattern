package com.zjx.DesignPattern.Structure.FacadePattern.impl;

import com.zjx.DesignPattern.Structure.FacadePattern.Shape;

public class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Rectangle::draw()");
	}

}
