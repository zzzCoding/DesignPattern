package com.zjx.DesignPattern.Structure.ProxyPattern;

public interface Image {

	void display();
}
