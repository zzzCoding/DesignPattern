package com.zjx.DesignPattern.Structure.ProxyPattern.impl;

import com.zjx.DesignPattern.Structure.ProxyPattern.Image;

public class ProxyImage implements Image {

	private RealImage realImage;
	private String fileName;

	public ProxyImage(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void display() {
		if (realImage == null) {
			realImage = new RealImage(fileName);
		}
		realImage.display();
	}
}