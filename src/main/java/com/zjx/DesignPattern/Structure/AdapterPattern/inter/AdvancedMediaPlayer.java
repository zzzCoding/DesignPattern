package com.zjx.DesignPattern.Structure.AdapterPattern.inter;

public interface AdvancedMediaPlayer {

	public void playVlc(String fileName);

	public void playMp4(String fileName);
}
