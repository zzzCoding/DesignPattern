package com.zjx.DesignPattern.Structure.AdapterPattern;

import com.zjx.DesignPattern.Structure.AdapterPattern.impl.Mp4Player;
import com.zjx.DesignPattern.Structure.AdapterPattern.impl.VlcPlayer;
import com.zjx.DesignPattern.Structure.AdapterPattern.inter.AdvancedMediaPlayer;
import com.zjx.DesignPattern.Structure.AdapterPattern.inter.MediaPlayer;

public class MediaAdapter implements MediaPlayer {

	AdvancedMediaPlayer advancedMusicPlayer;

	public MediaAdapter(String audioType) {
		if (audioType.equalsIgnoreCase("vlc")) {
			advancedMusicPlayer = new VlcPlayer();
		} else if (audioType.equalsIgnoreCase("mp4")) {
			advancedMusicPlayer = new Mp4Player();
		}
	}

	@Override
	public void play(String audioType, String fileName) {
		if (audioType.equalsIgnoreCase("vlc")) {
			advancedMusicPlayer.playVlc(fileName);
		} else if (audioType.equalsIgnoreCase("mp4")) {
			advancedMusicPlayer.playMp4(fileName);
		}
	}

}
