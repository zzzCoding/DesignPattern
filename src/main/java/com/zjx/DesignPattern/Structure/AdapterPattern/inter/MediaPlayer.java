package com.zjx.DesignPattern.Structure.AdapterPattern.inter;

public interface MediaPlayer {

	public void play(String audioType, String fileName);
}
