package com.zjx.DesignPattern.Structure.BridgePattern.impl;

import com.zjx.DesignPattern.Structure.BridgePattern.DrawAPI;

public class RedCircle implements DrawAPI {

	@Override
	public void drawCircle(int radius, int x, int y) {
		System.out.println("Drawing Circle[ color: red, radius: "
		         + radius +", x: " +x+", "+ y +"]");
	}

}
