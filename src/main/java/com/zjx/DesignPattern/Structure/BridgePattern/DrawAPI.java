package com.zjx.DesignPattern.Structure.BridgePattern;

public interface DrawAPI {

	public void drawCircle(int radius, int x, int y);
}
