package com.zjx.DesignPattern.结构型.装饰器;

public abstract class SuperCar implements ICar{

	private ICar car;
	
	public SuperCar(ICar car){
		this.car = car;
	}
	
	@Override
	public void move() {
		car.move();
	}

	
}
