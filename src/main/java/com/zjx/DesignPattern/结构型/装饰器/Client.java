package com.zjx.DesignPattern.结构型.装饰器;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Client {

/**
 *  装饰器模式（Decorator Pattern）允许向一个现有的对象添加新的功能，同时又不改变其结构。这种类型的设计模式属于结构型模式，它是作为现有的类的一个包装。
 * 
 *　　装饰器模式是一种用于代替继承的技术，无需通过继承增加子类就能扩展对象的新功能。使用对象的关联关系代替继承关系，更加灵活，同时避免类型体系的快速膨胀。<br>
 *	实现细节：<br>
 *	——Component抽象构件角色：真实对象和装饰对象有相同的接口。这样，客户端对象就能够以与真实对象相同的方式同装饰对象交互。<br>
 *	——ConcreteComponent具体构件角色（真实对象）：io流中的FileInputStream、FileOutputStream<br>
 *	——Decorator装饰角色：持有一个抽象构件的引用。装饰对象接受所有客户端的请求，并把这些请求转发给真实的对象。这样，就能在真实对象调用前后增加新的功能。<br>
 *	——ConcreteDecorator具体装饰角色：负责给构件对象增加新的责任。<br>
 *	
 *	IO中输入流和输出流的设计：<br>
 *	Swing包中图形界面构件功能<br>
 *	Servlet API中提供了一个request对象的Decorator设计模式的默认实现类HttpServletRequestWrapper，<br>
 *	HttpServletRequestWrapper类增强了request对象的功能。<br>
 * @throws FileNotFoundException 
 */
	public static void main(String[] args) throws FileNotFoundException {
		//InputStream i = new FileInputStream("");
		
		ICar c1 = new Car();
		c1.move();
		
		System.out.println("新增功能：天上飞");
		ICar flyCar = new FlyCar(c1);
		flyCar.move();
		
		System.out.println("增加新的功能：水里游");
		ICar waterCar = new WaterCar(c1);
		waterCar.move();

		System.out.println("增加两个新的功能，飞行，水里游");
		ICar aiCar = new AICar(c1);
		aiCar.move();
	}
}
