package com.zjx.DesignPattern.结构型.装饰器;

/**
 * ConcreteComponent 具体构件角色(真实对象)
 * @author zjx
 *
 */
public class Car implements ICar {

	@Override
	public void move() {
		System.out.println("陆地上跑！");
	}

}
