package com.zjx.DesignPattern.结构型.装饰器;

public class FlyCar extends SuperCar{

	public void fly(){
		System.out.println("天上飛！");
	}
	
	public FlyCar(ICar car) {
		super(car);
	}

	@Override
	public void move() {
		super.move();
		fly();
	}
}

class WaterCar extends SuperCar{

	public WaterCar(ICar car) {
		super(car);
	}
	
	public void swim(){
		System.out.println("水里游");
	}
	
	@Override
	public void move() {
		super.move();
		swim();
	}
}

class AICar extends SuperCar{

	public AICar(ICar car) {
		super(car);
	}
	
	public void autoMove(){
		System.out.println("自动跑");
	}
	
	@Override
	public void move() {
		super.move();
		autoMove();
	}
}
