package com.zjx.DesignPattern.结构型.适配器.intfadapter;

public class Source1 extends Wrapper{

	public void method1() {
		System.out.println("只关心method1");
	}

	public void method2() {
		System.out.println("只关心method2");
	}
}
