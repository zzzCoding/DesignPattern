package com.zjx.DesignPattern.结构型.适配器.objectadaper;

public interface AdapterInft {

	void method1();
	
	void method2();
}
