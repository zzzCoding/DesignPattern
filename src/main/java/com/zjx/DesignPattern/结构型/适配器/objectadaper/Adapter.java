package com.zjx.DesignPattern.结构型.适配器.objectadaper;


public class Adapter implements AdapterInft{

	Source source;
	
	public Adapter(Source source){
		this.source = source;
	}
	
	@Override
	public void method2() {
		System.out.println("第二期功能扩展，我要扩展功能！");
		
	}

	@Override
	public void method1() {
		source.method1();
	}

}
