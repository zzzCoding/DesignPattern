package com.zjx.DesignPattern.结构型.适配器.objectadaper;


/**
 * 对象适配
 * @author zjx
 *
 */
public class Test {

	public static void main(String[] args) {
		
		Source source = new Source();
		AdapterInft inf1 = new Adapter(source);
		inf1.method1();
		inf1.method2();
		
	}
}
