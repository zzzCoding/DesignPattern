package com.zjx.DesignPattern.结构型.适配器.intfadapter;

public interface Source {

	void method1();
	void method2();
	void method3();
	void method4();
	void method5();
	void method6();
	void method7();
	
}
