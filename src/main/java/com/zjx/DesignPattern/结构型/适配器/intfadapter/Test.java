package com.zjx.DesignPattern.结构型.适配器.intfadapter;

/**
 * 1、抽象类实现接口
 * 2、类继承抽象类
 * 3、类只重写用到的方法
 * @author zjx
 *
 */
public class Test {
	
	public static void main(String[] args) {
		Source source = new Source1();
		source.method1();
		source.method2();
	}
}
