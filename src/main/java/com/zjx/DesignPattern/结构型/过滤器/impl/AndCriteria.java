package com.zjx.DesignPattern.结构型.过滤器.impl;

import java.util.List;

import com.zjx.DesignPattern.结构型.过滤器.Criteria;
import com.zjx.DesignPattern.结构型.过滤器.entity.Person;

public class AndCriteria implements Criteria{

	private Criteria criteria;
	private Criteria otherCriteria;

	public AndCriteria(Criteria criteria, Criteria otherCriteria) {
		this.criteria = criteria;
		this.otherCriteria = otherCriteria;
	}

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> firstCriteriaPersons = criteria.meetCriteria(persons);
		return otherCriteria.meetCriteria(firstCriteriaPersons);
	}

}
