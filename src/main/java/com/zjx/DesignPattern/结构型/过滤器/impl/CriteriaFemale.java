package com.zjx.DesignPattern.结构型.过滤器.impl;

import java.util.ArrayList;
import java.util.List;

import com.zjx.DesignPattern.结构型.过滤器.Criteria;
import com.zjx.DesignPattern.结构型.过滤器.entity.Person;

public class CriteriaFemale implements Criteria {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> personList = new ArrayList<Person>();
		if(persons!= null && persons.size()>0){
			for (Person p : persons) {
				if (p.getGender().equalsIgnoreCase("FEMALE")) {
					personList.add(p);
				}
			}
		}
		
		return personList;
	}

}
