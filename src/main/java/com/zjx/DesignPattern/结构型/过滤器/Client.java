package com.zjx.DesignPattern.结构型.过滤器;

import java.util.ArrayList;
import java.util.List;

import com.zjx.DesignPattern.结构型.过滤器.entity.Person;
import com.zjx.DesignPattern.结构型.过滤器.impl.OrCriteria;
import com.zjx.DesignPattern.结构型.过滤器.impl.CriteriaFemale;
import com.zjx.DesignPattern.结构型.过滤器.impl.CriteriaMale;
import com.zjx.DesignPattern.结构型.过滤器.impl.CriteriaSingle;


public class Client {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<Person>();

		persons.add(new Person("Robert", "Male", "Single"));
		persons.add(new Person("John", "Male", "Married"));
		persons.add(new Person("Laura", "Female", "Married"));
		persons.add(new Person("Diana", "Female", "Single"));
		persons.add(new Person("Mike", "Male", "Single"));
		persons.add(new Person("Bobby", "Male", "Single"));
		
		CriteriaMale male = new CriteriaMale();
		CriteriaFemale female = new CriteriaFemale();
		CriteriaSingle single = new CriteriaSingle();
		
		System.out.println("\nSingle or male: ");
		OrCriteria singleMale = new OrCriteria(single, male);
		List<Person> p1 = singleMale.meetCriteria(persons);
		printPersons(p1);
		
		System.out.println("\nSingle or females: ");
		OrCriteria singleFemale = new OrCriteria(single, female);
		List<Person> p2 = singleFemale.meetCriteria(persons);
		printPersons(p2);
		
		
	}
	
	public static void printPersons(List<Person> persons) {
		for (Person person : persons) {
			System.out.println("Person : [ Name : " + person.getName()
					+ ", Gender : " + person.getGender()
					+ ", Marital Status : " + person.getMaritalStatus() + " ]");
		}
	}
	
}
