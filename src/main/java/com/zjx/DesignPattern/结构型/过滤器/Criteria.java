package com.zjx.DesignPattern.结构型.过滤器;

import java.util.List;

import com.zjx.DesignPattern.结构型.过滤器.entity.Person;


public interface Criteria {

	public List<Person> meetCriteria(List<Person> persons);
}
