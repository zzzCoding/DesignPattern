package com.zjx.DesignPattern.Create.FactoryPattern.inter;

public interface Shape {

	void draw();
}
