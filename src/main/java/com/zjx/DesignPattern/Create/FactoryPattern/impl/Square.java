package com.zjx.DesignPattern.Create.FactoryPattern.impl;

import com.zjx.DesignPattern.Create.FactoryPattern.inter.Shape;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method.");
	}

}
