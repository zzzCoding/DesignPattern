package com.zjx.DesignPattern.Create.FactoryPattern.impl;

import com.zjx.DesignPattern.Create.FactoryPattern.inter.Shape;

public class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
	}

}
