package com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.impl;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter.Color;

public class Green implements Color {

	@Override
	public void fill() {
		System.out.println("Inside Green::fill() method.");
	}

}
