package com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter;

public interface Color {

	void fill();
}
