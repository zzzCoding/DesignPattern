package com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter;

public interface Shape {

	void draw();
}
