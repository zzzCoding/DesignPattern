package com.zjx.DesignPattern.Create.AbstractFactoryPattern;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.impl.Blue;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.impl.Green;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.impl.Red;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter.Color;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter.Shape;

public class ColorFactory extends AbstractFactory{

	@Override
	public Color getColor(String color) {
		if(color == null){
	         return null;
	    }		
	    if(color.equalsIgnoreCase("RED")){
	         return new Red();
	    } else if(color.equalsIgnoreCase("GREEN")){
	         return new Green();
	    } else if(color.equalsIgnoreCase("BLUE")){
	         return new Blue();
	    }
	    return null;
	}

	@Override
	public Shape getShape(String shape) {
		return null;
	}

}
