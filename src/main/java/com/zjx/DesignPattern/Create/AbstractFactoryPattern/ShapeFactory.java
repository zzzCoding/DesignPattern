package com.zjx.DesignPattern.Create.AbstractFactoryPattern;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter.Color;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.impl.Circle;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.impl.Rectangle;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.impl.Square;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter.Shape;

public class ShapeFactory extends AbstractFactory{

	@Override
	public Color getColor(String color) {
		return null;
	}

	@Override
	public Shape getShape(String shapeType) {
		if(shapeType == null){
	         return null;
	      }		
	      if(shapeType.equalsIgnoreCase("CIRCLE")){
	         return new Circle();
	      } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
	         return new Rectangle();
	      } else if(shapeType.equalsIgnoreCase("SQUARE")){
	         return new Square();
	      }
	      return null;
	}

}
