package com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.impl;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter.Color;

public class Blue implements Color {

	@Override
	public void fill() {
		System.out.println("Inside Blue::fill() method.");
	}

}
