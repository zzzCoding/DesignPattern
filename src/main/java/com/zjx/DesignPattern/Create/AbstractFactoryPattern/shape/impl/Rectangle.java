package com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.impl;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter.Shape;


public class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
	}

}
