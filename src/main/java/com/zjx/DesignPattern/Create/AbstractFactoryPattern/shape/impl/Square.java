package com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.impl;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter.Shape;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method.");
	}

}
