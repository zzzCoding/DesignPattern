package com.zjx.DesignPattern.Create.AbstractFactoryPattern;

import com.zjx.DesignPattern.Create.AbstractFactoryPattern.color.inter.Color;
import com.zjx.DesignPattern.Create.AbstractFactoryPattern.shape.inter.Shape;

public abstract class AbstractFactory {

	public abstract Color getColor(String color);

	public abstract Shape getShape(String shape);
}
