package com.zjx.DesignPattern.Create.BuilderPattern.impl.burger;

import com.zjx.DesignPattern.Create.BuilderPattern.impl.Packing.Wrapper;
import com.zjx.DesignPattern.Create.BuilderPattern.inter.Item;
import com.zjx.DesignPattern.Create.BuilderPattern.inter.Packing;

public abstract class Burger implements Item {

	@Override
	public Packing packing() {
		return new Wrapper();
	}

	@Override
	public abstract float price();

}
