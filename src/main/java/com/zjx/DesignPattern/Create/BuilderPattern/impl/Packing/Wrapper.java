package com.zjx.DesignPattern.Create.BuilderPattern.impl.Packing;

import com.zjx.DesignPattern.Create.BuilderPattern.inter.Packing;

public class Wrapper implements Packing {

	@Override
	public String pack() {
		return "Wrapper";
	}

}
