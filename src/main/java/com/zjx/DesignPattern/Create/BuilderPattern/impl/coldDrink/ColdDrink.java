package com.zjx.DesignPattern.Create.BuilderPattern.impl.coldDrink;

import com.zjx.DesignPattern.Create.BuilderPattern.impl.Packing.Bottle;
import com.zjx.DesignPattern.Create.BuilderPattern.inter.Item;
import com.zjx.DesignPattern.Create.BuilderPattern.inter.Packing;

public abstract class ColdDrink implements Item {

	@Override
	public Packing packing() {
       return new Bottle();
	}

	@Override
	public abstract float price();

}
