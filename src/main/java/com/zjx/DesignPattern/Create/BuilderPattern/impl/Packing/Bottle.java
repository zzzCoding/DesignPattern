package com.zjx.DesignPattern.Create.BuilderPattern.impl.Packing;

import com.zjx.DesignPattern.Create.BuilderPattern.inter.Packing;

public class Bottle implements Packing {

	@Override
	public String pack() {
		return "Bottle";
	}

}
