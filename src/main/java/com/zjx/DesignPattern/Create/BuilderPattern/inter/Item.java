package com.zjx.DesignPattern.Create.BuilderPattern.inter;

public interface Item {

	String name();

	Packing packing();

	float price();
}
