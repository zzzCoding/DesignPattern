package com.zjx.DesignPattern.Create.BuilderPattern.inter;

public interface Packing {

	public String pack();
}
