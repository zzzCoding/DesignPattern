package com.zjx.DesignPattern.Create.BuilderPattern.impl.coldDrink;



public class Coke extends ColdDrink {

	@Override
	public float price() {
		return 30.0f;
	}

	@Override
	public String name() {
		return "Coke";
	}

}
