package com.zjx.DesignPattern.Create.BuilderPattern;

import com.zjx.DesignPattern.Create.BuilderPattern.impl.burger.ChickenBurger;
import com.zjx.DesignPattern.Create.BuilderPattern.impl.burger.VegBurger;
import com.zjx.DesignPattern.Create.BuilderPattern.impl.coldDrink.Coke;
import com.zjx.DesignPattern.Create.BuilderPattern.impl.coldDrink.Pepsi;

public class MealBuilder {

	public Meal prepareVegMeal() {
		Meal meal = new Meal();
		meal.addItem(new VegBurger());
		meal.addItem(new Coke());
		return meal;
	}

	public Meal prepareNonVegMeal() {
		Meal meal = new Meal();
		meal.addItem(new ChickenBurger());
		meal.addItem(new Pepsi());
		return meal;
	}
}
