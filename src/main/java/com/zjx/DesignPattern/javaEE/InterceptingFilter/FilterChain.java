/**
 * @Project apacheTest
 * @File FilterChain.java
 * @Package com.zzz.pattern.InterceptingFilter
 * @Date 2017年2月8日 下午5:10:26
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FilterChain
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:10:26
 */
public class FilterChain {
	private List<Filter> filters = new ArrayList<Filter>();
	private Target target;

	public void addFilter(Filter filter) {
		filters.add(filter);
	}

	public void execute(String request) {
		for (Filter filter : filters) {
			filter.execute(request);
		}
		target.execute(request);
	}

	public void setTarget(Target target) {
		this.target = target;
	}
}
