/**
 * @Project apacheTest
 * @File DebugFilter.java
 * @Package com.zzz.pattern.InterceptingFilter.filter
 * @Date 2017年2月8日 下午5:09:44
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter.filter;

import com.zjx.DesignPattern.javaEE.InterceptingFilter.Filter;

/**
 * @ClassName DebugFilter
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:09:44
 */
public class DebugFilter implements Filter {
	public void execute(String request) {
		System.out.println("request log: " + request);
	}

}
