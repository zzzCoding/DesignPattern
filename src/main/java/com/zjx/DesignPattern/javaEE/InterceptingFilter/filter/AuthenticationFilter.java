/**
 * @Project apacheTest
 * @File AuthenticationFilter.java
 * @Package com.zzz.pattern.InterceptingFilter.filter
 * @Date 2017年2月8日 下午5:09:22
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter.filter;

import com.zjx.DesignPattern.javaEE.InterceptingFilter.Filter;

/**
 * @ClassName AuthenticationFilter
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:09:22
 */
public class AuthenticationFilter implements Filter {
	public void execute(String request) {
		System.out.println("Authenticating request: " + request);
	}

}
