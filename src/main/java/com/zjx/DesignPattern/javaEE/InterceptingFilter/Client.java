/**
 * @Project apacheTest
 * @File Client.java
 * @Package com.zzz.pattern.InterceptingFilter
 * @Date 2017年2月8日 下午5:11:55
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter;
/**
 * @ClassName Client
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:11:55
 */
public class Client {
	FilterManager filterManager;

	public void setFilterManager(FilterManager filterManager) {
		this.filterManager = filterManager;
	}

	public void sendRequest(String request) {
		filterManager.filterRequest(request);
	}
}
