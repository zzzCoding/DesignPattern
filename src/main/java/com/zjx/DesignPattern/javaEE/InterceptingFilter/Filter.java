/**
 * @Project apacheTest
 * @File Filter.java
 * @Package com.zzz.pattern.InterceptingFilter
 * @Date 2017年2月8日 下午5:09:01
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.InterceptingFilter;
/**
 * @ClassName Filter
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:09:01
 */
public interface Filter {
	public void execute(String request);
}
