/**
 * @Project apacheTest
 * @File StudentVO.java
 * @Package com.zzz.pattern.TransferObject
 * @Date 2017年2月8日 下午5:35:14
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.TransferObject;
/**
 * @ClassName StudentVO
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:35:14
 */
public class StudentVO {
	private String name;
	private int rollNo;

	public StudentVO(String name, int rollNo) {
		this.name = name;
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
}
