/**
 * @Project apacheTest
 * @File StudentDaoImpl.java
 * @Package com.zzz.pattern.DataAccessObject.impl
 * @Date 2017年2月8日 下午4:24:26
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.DataAccessObject.impl;

import java.util.ArrayList;
import java.util.List;

import com.zjx.DesignPattern.javaEE.DataAccessObject.Student;
import com.zjx.DesignPattern.javaEE.DataAccessObject.StudentDao;

/**
 * @ClassName StudentDaoImpl
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:24:26
 */
public class StudentDaoImpl implements StudentDao {
	// 列表是当作一个数据库
	List<Student> students;

	public StudentDaoImpl() {
		students = new ArrayList<Student>();
		Student student1 = new Student("Robert", 0);
		Student student2 = new Student("John", 1);
		students.add(student1);
		students.add(student2);
	}

	@Override
	public void deleteStudent(Student student) {
		students.remove(student.getRollNo());
		System.out.println("Student: Roll No " + student.getRollNo() + ", deleted from database");
	}

	// 从数据库中检索学生名单
	@Override
	public List<Student> getAllStudents() {
		return students;
	}

	@Override
	public Student getStudent(int rollNo) {
		return students.get(rollNo);
	}

	@Override
	public void updateStudent(Student student) {
		students.get(student.getRollNo()).setName(student.getName());
		System.out.println("Student: Roll No " + student.getRollNo() + ", updated in the database");
	}

}
