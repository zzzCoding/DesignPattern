/**
 * @Project apacheTest
 * @File StudentDao.java
 * @Package com.zzz.pattern.DataAccessObject
 * @Date 2017年2月8日 下午4:24:06
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.DataAccessObject;

import java.util.List;

/**
 * @ClassName StudentDao
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:24:06
 */
public interface StudentDao {
	public List<Student> getAllStudents();

	public Student getStudent(int rollNo);

	public void updateStudent(Student student);

	public void deleteStudent(Student student);
}
