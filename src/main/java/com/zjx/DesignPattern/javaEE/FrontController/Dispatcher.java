/**
 * @Project apacheTest
 * @File Dispatcher.java
 * @Package com.zzz.pattern.FrontController
 * @Date 2017年2月8日 下午4:34:20
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.FrontController;

import com.zjx.DesignPattern.javaEE.FrontController.view.HomeView;
import com.zjx.DesignPattern.javaEE.FrontController.view.StudentView;

/**
 * @ClassName Dispatcher
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:34:20
 */
public class Dispatcher {
	private StudentView studentView;
	private HomeView homeView;

	public Dispatcher() {
		studentView = new StudentView();
		homeView = new HomeView();
	}

	public void dispatch(String request) {
		if (request.equalsIgnoreCase("STUDENT")) {
			studentView.show();
		} else {
			homeView.show();
		}
	}
}
