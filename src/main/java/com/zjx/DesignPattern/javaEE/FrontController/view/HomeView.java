/**
 * @Project apacheTest
 * @File HomeView.java
 * @Package com.zzz.pattern.FrontController
 * @Date 2017年2月8日 下午4:33:49
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.FrontController.view;
/**
 * @ClassName HomeView
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:33:49
 */
public class HomeView {
	public void show() {
		System.out.println("Displaying Home Page");
	}
}
