/**
 * @Project apacheTest
 * @File StudentView.java
 * @Package com.zzz.pattern.MVC
 * @Date 2017年2月8日 下午2:57:41
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.MVC;
/**
 * @ClassName StudentView
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午2:57:41
 */
public class StudentView {

	public void printStudentDetails(String studentName, String studentRollNo) {
		System.out.println("Student: ");
		System.out.println("Name: " + studentName);
		System.out.println("Roll No: " + studentRollNo);
	}
}
