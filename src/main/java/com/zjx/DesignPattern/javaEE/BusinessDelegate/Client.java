/**
 * @Project apacheTest
 * @File Client.java
 * @Package com.zzz.pattern.BusinessDelegate
 * @Date 2017年2月8日 下午3:13:20
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.BusinessDelegate;
/**
 * @ClassName Client
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:13:20
 */
public class Client {
	BusinessDelegate businessService;

	public Client(BusinessDelegate businessService) {
		this.businessService = businessService;
	}

	public void doTask() {
		businessService.doTask();
	}
}
