/**
 * @Project apacheTest
 * @File EJBService.java
 * @Package com.zzz.pattern.BusinessDelegate
 * @Date 2017年2月8日 下午3:10:43
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.BusinessDelegate;
/**
 * @ClassName EJBService
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:10:43
 */
public class EJBService implements BusinessService {

	@Override
	public void doProcessing() {
		System.out.println("Processing task by invoking EJB Service");
	}

}
