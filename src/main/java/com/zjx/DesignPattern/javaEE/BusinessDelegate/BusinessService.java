/**
 * @Project apacheTest
 * @File BusinessService.java
 * @Package com.zzz.pattern.BusinessDelegate
 * @Date 2017年2月8日 下午3:10:23
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.BusinessDelegate;
/**
 * @ClassName BusinessService
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:10:23
 */
public interface BusinessService {
	public void doProcessing();
}
