/**
 * @Project apacheTest
 * @File Service2.java
 * @Package com.zzz.pattern.ServiceLocator.impl
 * @Date 2017年2月8日 下午5:26:06
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.ServiceLocator.impl;

import com.zjx.DesignPattern.javaEE.ServiceLocator.Service;

/**
 * @ClassName Service2
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:26:06
 */
public class Service2 implements Service {

	public void execute() {
		System.out.println("Executing Service2");
	}

	@Override
	public String getName() {
		return "Service2";
	}

}
