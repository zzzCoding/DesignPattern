/**
 * @Project apacheTest
 * @File InitialContext.java
 * @Package com.zzz.pattern.ServiceLocator
 * @Date 2017年2月8日 下午5:26:34
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.ServiceLocator;

import com.zjx.DesignPattern.javaEE.ServiceLocator.impl.Service1;
import com.zjx.DesignPattern.javaEE.ServiceLocator.impl.Service2;


/**
 * @ClassName InitialContext
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:26:34
 */
public class InitialContext {
	public Object lookup(String jndiName) {
		if (jndiName.equalsIgnoreCase("SERVICE1")) {
			System.out.println("Looking up and creating a new Service1 object");
			return new Service1();
		} else if (jndiName.equalsIgnoreCase("SERVICE2")) {
			System.out.println("Looking up and creating a new Service2 object");
			return new Service2();
		}
		return null;
	}
}
