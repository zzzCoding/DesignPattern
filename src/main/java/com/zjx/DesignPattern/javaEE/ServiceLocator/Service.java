/**
 * @Project apacheTest
 * @File Service.java
 * @Package com.zzz.pattern.ServiceLocator
 * @Date 2017年2月8日 下午5:25:17
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.ServiceLocator;
/**
 * @ClassName Service
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午5:25:17
 */
public interface Service {
	public String getName();
	public void execute();
}
