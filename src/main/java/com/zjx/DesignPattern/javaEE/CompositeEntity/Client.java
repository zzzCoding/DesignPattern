/**
 * @Project apacheTest
 * @File Client.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午4:01:45
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity;

import com.zjx.DesignPattern.javaEE.CompositeEntity.entity.CompositeEntity;


/**
 * @ClassName Client
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:01:45
 */
public class Client {
	private CompositeEntity compositeEntity = new CompositeEntity();

	public void printData() {
		for (int i = 0; i < compositeEntity.getData().length; i++) {
			System.out.println("Data: " + compositeEntity.getData()[i]);
		}
	}

	public void setData(String data1, String data2) {
		compositeEntity.setData(data1, data2);
	}
}
