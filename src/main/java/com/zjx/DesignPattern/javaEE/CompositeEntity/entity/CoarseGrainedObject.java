/**
 * @Project apacheTest
 * @File CoarseGrainedObject.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午3:57:47
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity.entity;


/**
 * @ClassName CoarseGrainedObject
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:57:47
 */
public class CoarseGrainedObject {

	DependentObject1 do1 = new DependentObject1();
	DependentObject2 do2 = new DependentObject2();

	public void setData(String data1, String data2) {
		do1.setData(data1);
		do2.setData(data2);
	}

	public String[] getData() {
		return new String[] { do1.getData(), do2.getData() };
	}
}
