/**
 * @Project apacheTest
 * @File DependentObject1.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午3:57:01
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity.entity;

/**
 * @ClassName DependentObject1
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:57:01
 */
public class DependentObject1 {
	private String data;

	public void setData(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}
}
