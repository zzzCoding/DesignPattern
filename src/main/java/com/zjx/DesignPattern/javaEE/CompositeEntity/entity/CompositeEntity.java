/**
 * @Project apacheTest
 * @File CompositeEntity.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午4:01:22
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity.entity;


/**
 * @ClassName CompositeEntity
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午4:01:22
 */
public class CompositeEntity {
	private CoarseGrainedObject cgo = new CoarseGrainedObject();

	public void setData(String data1, String data2) {
		cgo.setData(data1, data2);
	}

	public String[] getData() {
		return cgo.getData();
	}
}
