/**
 * @Project apacheTest
 * @File DependentObject2.java
 * @Package com.zzz.pattern.CompositeEntity
 * @Date 2017年2月8日 下午3:57:16
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.javaEE.CompositeEntity.entity;

/**
 * @ClassName DependentObject2
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午3:57:16
 */
public class DependentObject2 {
	private String data;

	public void setData(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}
}
