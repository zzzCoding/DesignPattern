/**
 * @Project apacheTest
 * @File Observer.java
 * @Package com.zzz.pattern.Observer
 * @Date 2017年2月8日 下午2:22:05
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Observer;
/**
 * @ClassName Observer
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午2:22:05
 */
public abstract class Observer {
	protected Subject subject;
	public abstract void update();
}
