/**
 * @Project apacheTest
 * @File HexaObserver.java
 * @Package com.zzz.pattern.Observer
 * @Date 2017年2月8日 下午2:23:46
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Observer;
/**
 * @ClassName HexaObserver
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午2:23:46
 */
public class HexaObserver extends Observer {

	public HexaObserver(Subject subject) {
		this.subject = subject;
		this.subject.attach(this);
	}

	@Override
	public void update() {
		System.out.println("Hex String: " + Integer.toHexString(subject.getState()).toUpperCase());
	}

}
