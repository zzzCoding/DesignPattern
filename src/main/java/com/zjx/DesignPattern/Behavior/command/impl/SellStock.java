/**
 * @Project apacheTest
 * @File SellStock.java
 * @Package com.zzz.pattern.command.impl
 * @Date 2017年2月8日 上午10:34:02
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.command.impl;

import com.zjx.DesignPattern.Behavior.command.Order;
import com.zjx.DesignPattern.Behavior.command.Stock;

/**
 * @ClassName SellStock
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:34:02
 */
public class SellStock implements Order {
	private Stock abcStock;

	public SellStock(Stock abcStock) {
		this.abcStock = abcStock;
	}

	public void execute() {
		abcStock.sell();
	}

}
