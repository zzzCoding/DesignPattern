/**
 * @Project apacheTest
 * @File Broker.java
 * @Package com.zzz.pattern.command
 * @Date 2017年2月8日 上午10:36:23
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Broker
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:36:23
 */
public class Broker {
	private List<Order> orderList = new ArrayList<Order>();

	public void takeOrder(Order order) {
		orderList.add(order);
	}

	public void placeOrders() {
		for (Order order : orderList) {
			order.execute();
		}
		orderList.clear();
	}
}
