/**
 * @Project apacheTest
 * @File Order.java
 * @Package com.zzz.pattern.command
 * @Date 2017年2月8日 上午10:32:21
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.command;
/**
 * @ClassName Order
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:32:21
 */
public interface Order {
	void execute();
}
