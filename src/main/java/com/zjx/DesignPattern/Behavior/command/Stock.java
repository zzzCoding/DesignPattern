/**
 * @Project apacheTest
 * @File Stock.java
 * @Package com.zzz.pattern.command
 * @Date 2017年2月8日 上午10:32:40
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.command;

/**
 * @ClassName Stock
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:32:40
 */
public class Stock {

	private String name = "ABC";
	private int quantity = 10;

	public void buy() {
		System.out.println("Stock [ Name: " + name + ",  Quantity: " + quantity + " ] bought");
	}

	public void sell() {
		System.out.println("Stock [ Name: " + name + ",  Quantity: " + quantity + " ] sold");
	}

	public void other() {
		System.out.println("Stock [ Name: " + name + ",  Quantity: " + quantity + " ] other");
	}

}
