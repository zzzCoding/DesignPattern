/**
 * @Project apacheTest
 * @File CareTaker.java
 * @Package com.zzz.pattern.Memento
 * @Date 2017年2月8日 下午1:52:03
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Memento;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CareTaker
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午1:52:03
 */
public class CareTaker {
	private List<Memento> mementoList = new ArrayList<Memento>();

	public void add(Memento state) {
		mementoList.add(state);
	}

	public Memento get(int index) {
		return mementoList.get(index);
	}
}
