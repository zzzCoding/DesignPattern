/**
 * @Project apacheTest
 * @File Memento.java
 * @Package com.zzz.pattern.Memento
 * @Date 2017年2月8日 下午1:51:16
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Memento;

/**
 * @ClassName Memento
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午1:51:16
 */
public class Memento {
	private String state;

	public Memento(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}
}
