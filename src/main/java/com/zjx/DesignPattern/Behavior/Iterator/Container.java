/**
 * @Project apacheTest
 * @File Container.java
 * @Package com.zzz.pattern.Iterator
 * @Date 2017年2月8日 上午11:24:37
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Iterator;
/**
 * @ClassName Container
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:24:37
 */
public interface Container {
	public Iterator getIterator();
}
