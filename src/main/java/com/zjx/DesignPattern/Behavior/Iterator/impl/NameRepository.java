/**
 * @Project apacheTest
 * @File NameRepository.java
 * @Package com.zzz.pattern.Iterator.impl
 * @Date 2017年2月8日 上午11:25:51
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Iterator.impl;

import com.zjx.DesignPattern.Behavior.Iterator.Container;
import com.zjx.DesignPattern.Behavior.Iterator.Iterator;

/**
 * @ClassName NameRepository
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:25:51
 */
public class NameRepository implements Container {
	public String names[] = { "Robert", "John", "Julie", "Lora" };

	@Override
	public Iterator getIterator() {
		return new NameIterator();
	}

	private class NameIterator implements Iterator {
		int index;

		@Override
		public boolean hasNext() {
			if (index < names.length) {
				return true;
			}
			return false;
		}

		@Override
		public Object next() {
			if (this.hasNext()) {
				return names[index++];
			}
			return null;
		}
	}

}
