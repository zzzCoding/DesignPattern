/**
 * @Project apacheTest
 * @File User.java
 * @Package com.zzz.pattern.Mediator.entity
 * @Date 2017年2月8日 下午12:00:35
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Mediator.entity;

import com.zjx.DesignPattern.Behavior.Mediator.ChatRoom;

/**
 * @ClassName User
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午12:00:35
 */
public class User {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User(String name) {
		this.name = name;
	}

	public void sendMessage(String message) {
		ChatRoom.showMessage(this, message);
	}
}
