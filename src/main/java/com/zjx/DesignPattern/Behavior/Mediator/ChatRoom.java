/**
 * @Project apacheTest
 * @File ChatRoom.java
 * @Package com.zzz.pattern.Mediator
 * @Date 2017年2月8日 下午12:00:13
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.Mediator;

import java.util.Date;

import com.zjx.DesignPattern.Behavior.Mediator.entity.User;

/**
 * @ClassName ChatRoom
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 下午12:00:13
 */
public class ChatRoom {

	public static void showMessage(User user, String message) {
		System.out.println(new Date().toString() + " [" + user.getName() + "] : " + message);
	}
}
