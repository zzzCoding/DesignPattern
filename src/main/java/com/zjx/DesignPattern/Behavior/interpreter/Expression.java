/**
 * @Project apacheTest
 * @File Expression.java
 * @Package com.zzz.pattern.Interpreter
 * @Date 2017年2月8日 上午11:00:01
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.interpreter;
/**
 * @ClassName Expression
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:00:01
 */
public interface Expression {
	public boolean interpret(String context);
}
