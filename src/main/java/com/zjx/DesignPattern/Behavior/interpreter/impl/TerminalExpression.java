/**
 * @Project apacheTest
 * @File TerminalExpression.java
 * @Package com.zzz.pattern.Interpreter
 * @Date 2017年2月8日 上午11:00:19
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.interpreter.impl;

import com.zjx.DesignPattern.Behavior.interpreter.Expression;

/**
 * @ClassName TerminalExpression
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:00:19
 */
public class TerminalExpression implements Expression {

	private String data;

	public TerminalExpression(String data) {
		this.data = data;
	}

	@Override
	public boolean interpret(String context) {
		if (context.contains(data)) {
			return true;
		}
		return false;
	}

}
