/**
 * @Project apacheTest
 * @File AndExpression.java
 * @Package com.zzz.pattern.Interpreter.impl
 * @Date 2017年2月8日 上午11:02:34
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.interpreter.impl;

import com.zjx.DesignPattern.Behavior.interpreter.Expression;

/**
 * @ClassName AndExpression
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午11:02:34
 */
public class AndExpression implements Expression {

	private Expression expr1 = null;
	private Expression expr2 = null;

	public AndExpression(Expression expr1, Expression expr2) {
		this.expr1 = expr1;
		this.expr2 = expr2;
	}

	@Override
	public boolean interpret(String context) {
		return expr1.interpret(context) && expr2.interpret(context);
	}
}
