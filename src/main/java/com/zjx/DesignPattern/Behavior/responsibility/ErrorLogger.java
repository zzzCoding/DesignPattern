/**
 * @Project apacheTest
 * @File ErrorLogger.java
 * @Package com.zzz.responsibility
 * @Date 2017年2月8日 上午10:01:45
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.responsibility;
/**
 * @ClassName ErrorLogger
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:01:45
 */
public class ErrorLogger extends AbstractLogger {

	public ErrorLogger(int level) {
		this.level = level;
	}

	@Override
	protected void write(String message) {
		System.out.println("Error Console::Logger: " + message);
	}

}
