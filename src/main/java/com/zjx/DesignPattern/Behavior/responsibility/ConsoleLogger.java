/**
 * @Project apacheTest
 * @File ConsoleLogger.java
 * @Package com.zzz.responsibility
 * @Date 2017年2月8日 上午10:05:39
 * @Version V1.0
 * @Copyright (c) 2017, www.zhongzhihui.com All Rights Reserved.
 */

package com.zjx.DesignPattern.Behavior.responsibility;

/**
 * @ClassName ConsoleLogger
 * @Description TODO(用一句话描述这个类)
 * @author zhangjianxiang@zhongzhihui.com
 * @date 2017年2月8日 上午10:05:39
 */
public class ConsoleLogger extends AbstractLogger {

	public ConsoleLogger(int level) {
		this.level = level;
	}

	@Override
	protected void write(String message) {
		System.out.println("Standard Console::Logger: " + message);
	}

}
